`timescale 1ns/1ps

`include "xdefs.vh"
`include "xctrldefs.vh"
`include "xprogdefs.vh"
`include "xregfdefs.vh"

module gpo (
	    input 		     clk,
	    input 		     rst,
	    input 		     sel,

	    input reg [`DATA_W-1:0]  data_in,
	    output reg [`DATA_W-1:0] data_out, 		    
	    );

always @ (posedge clk) begin
   if(rst)begin
      data_out <= 1'b0;
   end  else
     if(sel)begin
	data_out <= data_in;
     end
end


endmodule
   
