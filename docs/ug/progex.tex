\section{Programming}
\label{sec:progex}

The Versat controller can only be programmed in assembly language, which
is not difficult due to its simple architecture. To better explain
how the ecosystem works, a full example will be used in this section
to show how to program and use the assembler, for an 8-bit controller.

\subsection{The Assembler}

The assembler is a Python script that reads the assembly code (in a
$<$file$>$.va) and translates it into machine code. It is design to
generate machine code for 2 different memories: the boot ROM memory
and the instruction memory. (Depending on the system where it is
inserted, the controller can have a program memory with a boot ROM and
an instruction memories, or just the boot ROM.)

The assembler generates two files, the opcode.hex and the rom.v. The
first one is just the machine code written as a text file (one
instruction per line) with the program memory size, which are read
with the {\em readmemh} (a verilog primitive). If the target memory
has a 256 instruction capacity, it will generate 256 machine codes,
the first instructions correspond to the ones given to the assembler
and remain are filled with {\tt nop} instructions (see
section~\ref{sec:isa}). The second one is a file that contains the
same instructions in a verilog hard ROM format, so it is possible to
use a ROM without the {\em readmemh}. This second file is crucial in
case of ASIC implementation and is only generated for the boot ROM.

To use the assembler, there is the need to create a dictionary named
xdict.txt with some parameters. This dictionary is a JSON file read by
the assembler, so it can generates the machine codes correctly. The
parameters needed are the following:
\begin{itemize}
        \item All the memory addresses (memory map);
        \item Instruction parameters: immediate and instruction widths
          (IMM\_W and INSTR\_W, respectively). Note: the INSTR\_W is
          equal to opcode size (4 bits) + IMM\_W;
        \item Boot ROM address width (ROM\_ADDR\_W);
        \item Instruction memory address width (IADDR\_W);
        \item Number of delay slots (DELAY\_SLOTS).
\end{itemize}

Some of these parameters may not be necessary, it depends on the
system where the controller is inserted.

For the boot ROM, the assembler should be invoked as follows:
\begin{verbatim}
./va -b < <assembly file>.va
\end{verbatim}

For the instruction memory, the assembler should be invoked as in the
boot ROM case, but without the boot ROM flag:
\begin{verbatim}
./va < <assembly file>.va
\end{verbatim}

\newpage

\subsection{Assembly}

\subsubsection{Program}

In figure~\ref{fig:as_code} is an example of a full assembly routine
for Versat controller.

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=.45\textwidth]{code}}
    \vspace{0cm}\caption{Read from SPI routine.}
    \label{fig:as_code}
\end{figure}

Due to the given dictionary, all mnemonics are recognized by the
assembler. For instance, the {\tt SPI\_CTRL\_REG} is a known address
from the assembler point of view.

This routine executes the SPI protocol for reading from external
modules, by reading and writing to a peripheral present on the memory
map. The comments are written after a ''{\tt \#}'' character is
inserted, they can be used in the beginning of a line, or at the end
of it.

The blue words are labels and the assembler use them to compute the
jump addresses.

The immediate value has half the width of the data. To load a value
bigger than that, the programmer should do as follows:
\begin{verbatim}
ldi 0x67
ldih 0x67
\end{verbatim}

The assembler will first read the less significant bits from value,
for the first instruction, and then will read the most significant
bits, for the second one. The prefix {\tt 0x} is used to indicate to
the assembler that it is a hexadecimal value.

When an immediate value is bigger than the IMM\_W parameter, the
programmer must use a shift as follows:
\begin{verbatim}
ldi 0x367>>8
\end{verbatim}

This shift will be executed at compile time by the assembler. It
performs the shift and then replaces the immediate in the instruction
by the result (in this example, $3$).

The same happens with labels, and because the programmer never knows
what value they have, it is imperative to use this scheme.

\begin{verbatim}
ldi rdloop
ldih rdloop
wrw RB
ldi rdloop>>8
wrw RB,1
\end{verbatim}

This last instruction is translated by the assembler as a write
instruction to the sum of {\tt RB} address plus one. To read or write
any value to a memory mapped address bigger than immediate width (in
this example, 4 bits), the instructions {\tt rdwb} and {\tt wrwb},
respectively, must be used.

For optimization, in a case of a {\tt for loop}, the delay slot is
filled with the write instruction to the control loop register
(see~\ref{sec:isa}). For example:
\begin{verbatim}
rdw R1
bneq
wrw R1
\end{verbatim}
where the {\tt R1} is the control register. Do not forget to load the
{\tt RB} with the label used in the {\tt for loop} before the branch
instruction.

\newpage

\subsubsection{Data}

In figure~\ref{fig:as_data} is an example of a data section in an
assembly program for Versat controller.

\begin{figure}[!htbp]
    \centerline{\includegraphics[width=.3\textwidth]{data}}
    \vspace{0cm}\caption{Data in the program memory.}
    \label{fig:as_data}
\end{figure}

Here, the data must be preceded from the directive {\tt .memset}, in
the instruction place. The use of a label is required to access the
data.
